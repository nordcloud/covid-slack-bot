import AWS from 'aws-sdk';
import middy from '@middy/core'
import cors from '@middy/http-cors'
import { defaultJson } from 'slack_bot_lib/middleware'

const dynamo = new AWS.DynamoDB.DocumentClient();

const handler = async event => {
  const [start, end] = event.pathParameters.range.split(',');
  if (!start || !end) {
    throw new Error('Invalid date');
  }

  const query = await dynamo.query({
    TableName: process.env.TABLE_NAME,
    IndexName: 'GSI1',
    KeyConditionExpression: 'sk = :sk AND #data BETWEEN :start AND :end',
    ExpressionAttributeValues: {
      ':sk': 'ANSWER',
      ':start': start,
      ':end': end,
    },
    ExpressionAttributeNames: {
      '#data': 'data',
    }
  }).promise();

  const answers = query.Items.map((i) => {
    let res = i;
    const [questionId, userId, date] = res.pk.split('#');

    // Clean up the output
    delete res.pk;
    delete res.sk;
    delete res.data;

    res = {
      ...res,
      questionId,
      userId,
      date,
    }

    return res;
  });

  return answers;
}

const lambda = middy(handler)
  .use(cors())
  .use(defaultJson())

export { lambda as handler }