import AWS from 'aws-sdk';
import middy from '@middy/core'
import cors from '@middy/http-cors'
import { defaultJson } from 'slack_bot_lib/middleware'

const dynamo = new AWS.DynamoDB.DocumentClient();

const handler = async event => {
  const query = await dynamo.query({
    TableName: process.env.TABLE_NAME,
    IndexName: 'GSI1',
    KeyConditionExpression: 'sk = :sk',
    ExpressionAttributeValues: {
      ':sk': 'QUESTION',
    },
  }).promise();

  const questions = query.Items.map((i) => {
    const res = i;
    res.id = i.pk;

    return res;
  });

  return questions;
}

const lambda = middy(handler)
  .use(cors())
  .use(defaultJson())

export { lambda as handler }