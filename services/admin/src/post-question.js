import AWS from 'aws-sdk';
import { uuid } from 'uuidv4';
import middy from '@middy/core'
import cors from '@middy/http-cors'
import jsonBodyParser from '@middy/http-json-body-parser';
import { defaultJson, boomErrorHandler } from 'slack_bot_lib/middleware'

const dynamo = new AWS.DynamoDB.DocumentClient();

const handler = async event => {
  const { question, type, options, } = event.body
  console.log(event.body);
  console.log(process.env.TABLE_NAME);

  if (!question) {
    throw new Error('Missing question') // TODO: Use Middy / throw error
    return;
  }

  const query = await dynamo.put({
    TableName: process.env.TABLE_NAME,
    Item: {
      pk: uuid(),
      sk: 'QUESTION',
      data: type,
      question,
      options,
    },
  }).promise();

  return query;
}

const lambda = middy(handler)
  .use(jsonBodyParser())
  .use(cors())
  .use(defaultJson())
  .user(boomErrorHandler)

export { lambda as handler }
