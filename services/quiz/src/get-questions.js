import AWS from 'aws-sdk';
import middy from '@middy/core'
import cors from '@middy/http-cors'
import { defaultJson } from 'slack_bot_lib/middleware'

const dynamo = new AWS.DynamoDB.DocumentClient();

const handler = async event => {
  const previousQuestion = event.pathParameters.previous;

  const query = await dynamo.query({
    TableName: process.env.TABLE_NAME,
    IndexName: 'GSI1',
    KeyConditionExpression: 'sk = :sk',
    ExpressionAttributeValues: {
      ':sk': 'QUESTION',
    },
  }).promise();

  const result = query.Items.filter(question => {
    question.id = question.pk;
    return previousQuestion !== question.id;
  });

  return result;
}

const lambda = middy(handler)
  .use(cors())
  .use(defaultJson())

export { lambda as handler }