import AWS from 'aws-sdk';
import middy from '@middy/core'
import cors from '@middy/http-cors'
import { defaultJson } from 'slack_bot_lib/middleware'

const dynamo = new AWS.DynamoDB.DocumentClient();

const handler = async event => {
  const query = await dynamo.get({
    TableName: process.env.TABLE_NAME,
    Key: {
      'pk': event.pathParameters.id,
      'sk': 'QUESTION',
    }
  }).promise();

  console.log(query);

  return query.Item;
}

const lambda = middy(handler)
  .use(cors())
  .use(defaultJson())

export { lambda as handler }