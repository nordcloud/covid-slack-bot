import AWS from 'aws-sdk';
import { uuid } from 'uuidv4';
import jwt from 'jsonwebtoken';
import middy from '@middy/core'
import cors from '@middy/http-cors'
import jsonBodyParser from '@middy/http-json-body-parser';
import { redirect, boomErrorHandler } from 'slack_bot_lib/middleware'

const dynamo = new AWS.DynamoDB.DocumentClient();
const { CLIENT_URL, JWT_SECRET } = process.env;

const handler = async event => {
  const token = event.pathParameters.token;
  try {
    const decoded = jwt.verify(token, JWT_SECRET);
    console.log(decoded);

    const { data: { userId, questionId, answer: { value } } } = decoded;

    console.log(token);

    if (!questionId) {
      throw new Error('Missing question') // TODO: Use Middy / throw error
    }

    await dynamo.put({
      TableName: process.env.TABLE_NAME,
      Item: {
        pk: `${questionId}#${userId}#${(new Date()).toISOString().split('T')[0]}`,
        sk: 'ANSWER',
        data: (new Date()).toISOString().split('T')[0],
        answer: value,
        createdAt: (new Date().getTime())
      },
    }).promise();

    // Redirect user to new question, with previous question in parameters
    return `https://${CLIENT_URL}/${userId}/${questionId}`;
  } catch (err) {
    console.log(err);
    throw new Error(e);
  }
}

const lambda = middy(handler)
  .use(jsonBodyParser())
  .use(cors())
  .use(redirect())
  .use(boomErrorHandler)

export { lambda as handler }