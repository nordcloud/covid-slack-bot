import AWS from 'aws-sdk';
import middy from '@middy/core'
import cors from '@middy/http-cors'
import jsonBodyParser from '@middy/http-json-body-parser';
import { defaultJson, boomErrorHandler } from 'slack_bot_lib/middleware'

const dynamo = new AWS.DynamoDB.DocumentClient();

const handler = async event => {
  const { userIds } = event.body;

  let returnObject = {};
  for (const userId of userIds) {
    const query = await dynamo.query({
      TableName: process.env.TABLE_NAME,
      IndexName: 'GSI1',
      KeyConditionExpression: "#data = :data and #sk = :sk",
      ExpressionAttributeNames: {
        "#data": "data",
        "#sk": "sk"
      },
      ExpressionAttributeValues: {
        ":data": "SELECT",
        ":sk": "QUESTION"
      }
    }).promise();

    const question = query.Items[Math.floor(Math.random() * query.Items.length)];
    question.id = question.pk;
    returnObject[userId] = question;
  };

  return returnObject;
}

const lambda = middy(handler)
  .use(jsonBodyParser())
  .use(cors())
  .use(defaultJson())
  .use(boomErrorHandler)

export { lambda as handler }