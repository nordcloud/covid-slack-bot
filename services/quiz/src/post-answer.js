import AWS from 'aws-sdk';
import { uuid } from 'uuidv4';
import middy from '@middy/core'
import cors from '@middy/http-cors'
import httpUrlEncodeBodyParser from '@middy/http-urlencode-body-parser';
import { defaultJson } from 'slack_bot_lib/middleware'

const dynamo = new AWS.DynamoDB.DocumentClient();

const handler = async event => {
  console.log(event);
  const questionId = event.pathParameters.id;
  const { answer, userId } = event.body

  console.log(event.body);

  if (!questionId) {
    return 'Missing question' // TODO: Use Middy / throw error
  }

  const query = await dynamo.put({
    TableName: process.env.TABLE_NAME,
    Item: {
      pk: `${questionId}#${userId}#${(new Date()).toISOString().split('T')[0]}`,
      sk: 'ANSWER',
      data: (new Date()).toISOString().split('T')[0],
      answer,
      createdAt: (new Date().getTime())
    },
  }).promise();

  return query;
}

const lambda = middy(handler)
  .use(httpUrlEncodeBodyParser({ extended: true }))
  .use(cors())
  .use(defaultJson())

export { lambda as handler }