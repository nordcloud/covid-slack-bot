'use strict'

import middy from '@middy/core'
import cors from '@middy/http-cors'
import { defaultJson, boomErrorHandler } from 'slack_bot_lib/middleware';
import { SlackUtil } from 'slack_bot_lib/utils/slack-util';

const handler = async event => {
  try {
    const util = new SlackUtil(process.env.slackToken);

    const { Records } = event;
    Records.forEach(record => {
      const data = JSON.parse(record.body);
      util.sendSlackMessage(data.userId, data.block)
    });
  }
  catch(e) {
    console.log(e)
    throw new Error(e)
  }
}

const lambda = middy(handler)
  .use(cors())
  .use(defaultJson())
  .use(boomErrorHandler)

export { lambda as handler }