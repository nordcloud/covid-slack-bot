'use strict'

import middy from '@middy/core'
import cors from '@middy/http-cors'
import axios from 'axios';
import { defaultJson, boomErrorHandler } from 'slack_bot_lib/middleware';
import { generateQuestion, getFirstQuestion } from 'slack_bot_lib/utils/question-util';
import { SlackUtil } from 'slack_bot_lib/utils/slack-util';
import AWS from "aws-sdk";

const sqs = new AWS.SQS({apiVersion: '2012-11-05'});

// This chunk a big array to multiple arrays
// sorry for the recursive function ¯\_(ツ)_/¯
const chunkArray = (array, maxPerSubArray) => {
  if(!array) return []; //safe check

  const firstChunk = array.slice(0, maxPerSubArray);
  if(firstChunk.length == 0) return []; // base condition to break recursive call

  const remaining = array.slice(maxPerSubArray, array.length)

  return [[...firstChunk], ...chunkArray(remaining, maxPerSubArray)];
} 

const handler = async event => {
  try {
    let delayInSecond = 0;
    const util = new SlackUtil(process.env.slackToken);
    const slackUsers = await util.fetchUserLists();
    const slackUserWithQuestion = await getFirstQuestion(slackUsers)

    const chunkedSlackUsers = chunkArray(slackUserWithQuestion, 10) // chunk array so can use sqs sendMessageBatch
    chunkedSlackUsers.forEach(async chunk => {
      const params = {
        QueueUrl: process.env.sqsURL,
        Entries: [],
      }
  
      chunk.forEach(user => {
        let block = generateQuestion(user.firstQuestion, user.hashId);
        params.Entries.push({
          Id: `${Date.now()}-${user.id}`,
          MessageBody: JSON.stringify({
            block:block,
            userId:user.id
          }),
          DelaySeconds: delayInSecond
        });
      })
      delayInSecond = delayInSecond + 60; // delay 1 mins for the next batch of message
      await sqs.sendMessageBatch(params).promise();
    });
  }
  catch(e) {
    console.log(e)
    throw new Error(e)
  }
}

const lambda = middy(handler)
  .use(cors())
  .use(defaultJson())
  .use(boomErrorHandler)

export { lambda as handler }