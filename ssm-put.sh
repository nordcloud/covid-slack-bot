#! /usr/bin/env bash

if [ "$1" != "" ]; then
    ENV=$1
else
    echo "Please add an environment to deploy (dev,testing,production)"
    exit 0
fi

if [ "$2" != "" ]; then
    PROFILE=$2
else
    echo "Please define profile to be used"
    exit 0
fi

token="XXXXX"
jwtSecret="XXXX"
salt="XXXX"

aws ssm put-parameter --cli-input-json "{\"Name\": \"/${ENV}/slacktoken\", \"Value\": \"${token}\", \"Type\": \"SecureString\"}" --overwrite --region eu-west-1 --profile ${PROFILE}
aws ssm put-parameter --cli-input-json "{\"Name\": \"/${ENV}/jwtsecret\", \"Value\": \"${jwtSecret}\", \"Type\": \"SecureString\"}" --overwrite --region eu-west-1 --profile ${PROFILE}
aws ssm put-parameter --cli-input-json "{\"Name\": \"/${ENV}/idsalt\", \"Value\": \"${salt}\", \"Type\": \"SecureString\"}" --overwrite --region eu-west-1 --profile ${PROFILE}
