import React from 'react';

export default () => (
  <div style={{ textAlign: 'center' }}>
    <h2>Thank you for your response!</h2>
    <p>
      You will receive new Wellness Survey on Slack tomorrow.
    </p>
  </div>
);
