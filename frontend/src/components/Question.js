import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router-dom";
import axios from 'axios';
import qs from 'qs';
import { Form, Input, Button, Spin, Checkbox } from 'antd';

const Question = (props) => {
  const { match: { params: { userId, questionId } } } = props;
  const history = useHistory();
  const [question, setQuestion] = useState(null);
  const [questions, setQuestions] = useState(null);
  const [loading, setLoading] = useState(false);

  const onFinish = async (values) => {
    setLoading(true);
    const answer = values[question.id];

    await axios.post(`${process.env.REACT_APP_NC_COVID_QUIZ_ENDPOINT}/answer/q/${question.id}`, qs.stringify({ userId, answer }),
      {
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
      }
    );

    if (questions.length > 1) {
      // Remove answered question for the list & choose next one
      questions.splice(questions.find(q => q.id === question.id), 1);
      setQuestion(questions[0]);
      setLoading(false);
    } else {
      // Current question was the last item in array, redirect to result view
      history.push("/result");
    }
  };


  useEffect(() => {
    async function fetchData() {
      setLoading(true);
      const res = await axios.get(`${process.env.REACT_APP_NC_COVID_QUIZ_ENDPOINT}/questions/${questionId}`);
      setLoading(false);
      setQuestion(res.data[0]);
      setQuestions(res.data);
    }
    fetchData();
  }, [questionId]);

  if (!question) {
    return <center><Spin size="large" /></center>;
  }

  return (
    <Form onFinish={onFinish} layout="vertical">
      {loading ? <center><Spin size="large" /></center> : (
        <>
          {question.data === 'TEXT' && (
            <Form.Item
              label={question.question}
              name={question.pk}
            >
              <Input.TextArea rows={4} />
            </Form.Item>
          )
          }

          {question.data === 'MULTISELECT' && (
            <Form.Item name={question.pk} label={question.question}>
              <Checkbox.Group>
                {question.options.map(q => (
                  <p key={q.key}><Checkbox value={q.key}>{q.value}</Checkbox></p>
                ))}
              </Checkbox.Group>
            </Form.Item>
          )}

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </>
      )}
    </Form>
  )
}

export default Question;