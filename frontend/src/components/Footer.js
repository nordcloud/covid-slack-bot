import React from 'react';
import { Layout } from 'antd';

const { Footer } = Layout;

export default () => (
  <Footer style={{ textAlign: 'center' }}>
    <p>
      All answers are anonymous
    </p>
    <p>
      About | Privacy
    </p>
    <p>
      &copy; Nordcloud
    </p>
  </Footer>
);