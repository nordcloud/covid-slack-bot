This is a repo for Nordcloud's health check bot. 
The project include:
   a lambda to trigger bot sending message
   frontend / backend web app to handle question CRUD


Brief project structure below:   
```
/ - root folder
│
└───frontend - Frontend application 
│
└───lib - shared library between services within project
│    │
│    └───dynamodb - dynamoDB mapper model and table classes
│    │
│    └───docs - Swagger and related docs
│    │
│    └───schemas - Joi Schema files
│    │
│    └───utils
│
└───services - Code for APIs
│    │
│    └───service 1
│    │    │
│    │    └───src
│    │    │     └───get.js
│    │    │     └───list.js
│    │    │     └───etc...
│    │    │
│    │    └───package.json
│    │    │
│    │    └───servless.yaml (each service is a separate CF stack)
│    │    │
│    │    └───webpack.config.js
│    │
│    └───service 2
│
└───tests - Tests for services or utils
```