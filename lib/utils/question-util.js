'use strict'

const boom = require('@hapi/boom');
const jwt = require('jsonwebtoken');
const axios = require('axios');

const template = [
  {
    "type": "section",
    "text": {
      "type": "mrkdwn",
      "text": "Hi, I'm a wellness bot. I will be in contact with you during this epidemic time. I will ask you a few questions. Here is your first question. Simply click on the options below to answer. "
    }
  },
  {
    "type": "section",
    "text": {
      "type": "plain_text",
      "text": ":lock: All of your answer are anonymous",
      "emoji": true
    }
  },
  {
    "type": "divider"
  }
]

const generateQuestion = (input, userId) => {
  const block = Object.assign([], template);
  const {question, options, pk} = input;

  //Question
  block.push(  {
    "type": "section",
    "text": {
      "type": "mrkdwn",
      "text": question
    }
  })
  
  const answerButtons = {
    "type": "actions",
    "elements": []
  }

  //Answer
  options.forEach(answer => {
    const token = jwt.sign({
      exp: Math.floor(Date.now() / 1000) + (4320 * 60), // token valid for 3 days
      data: {
        userId: userId,
        questionId: pk,
        answer: answer
      }
    }, process.env.jwtSecret)
    answerButtons.elements.push(
      {
        "type": "button",
        "text": {
          "type": "plain_text",
          "text": answer.value
        },
        "url": `${process.env.restAPI}/answer/${token}`
      }
    )
  });
  block.push(answerButtons);
  return block;
}

const getFirstQuestion = async userList => {
  const hashOnlyArray = userList.map(user => user.hashId);
  const firstAnswers = await axios.post(`${process.env.restAPI}/question/user/`, {userIds: hashOnlyArray});
  if(firstAnswers.data !== null) {
    userList.forEach(user => {
      const question = firstAnswers.data[user.hashId];
      user.firstQuestion = question
    })
  }
  return userList;
}


module.exports = { generateQuestion, getFirstQuestion }
