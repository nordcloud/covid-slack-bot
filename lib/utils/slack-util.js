'use strict'

const axios = require('axios');
const crypto = require('crypto');

class SlackUtil {
  constructor(slackToken) {
    this.token = slackToken
  }

  async fetchUserLists() {
    let cursor = "" ; // use for pagination purpose
    const limit = 200;

    let isPending = true;
    const slackUsers = [];


    while(isPending) {
      const response = await axios.get(`https://slack.com/api/users.list?limit=${limit}&cursor=${cursor}`, {
      headers: {
          Authorization: `Bearer ${this.token}`,
          'Content-Type': 'application/json',
          }
        }
      )
      const { members, response_metadata } = response.data;
      members.forEach(member => {
        if(!member.is_bot && !member.deleted && !member.is_restricted && !member.is_ultra_restricted) {
          slackUsers.push(
            {
              id:member.id,
              hashId: crypto.createHash('sha256').update(`${process.env.hashSalt}${member.id}`).digest('hex')
            }
          )
        }
      })

      if(response_metadata.next_cursor !== "") 
        cursor = response_metadata.next_cursor;
      else 
        isPending = false;
    }
    return slackUsers;
  }

  async sendSlackMessage(userId, blockMessage) {
      const response = await axios.post('https://slack.com/api/chat.postMessage',
        {
          channel: userId,
          blocks: blockMessage
        }
        ,{
        headers: {
          Authorization: `Bearer ${this.token}`,
          'Content-Type': 'application/json',
        }
      });
  }
}

module.exports = { SlackUtil }
